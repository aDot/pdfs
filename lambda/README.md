## Lambda Papers

### SCHEME: An Interpreter for Extended Lambda Calculus

[DSpace](https://dspace.mit.edu/handle/1721.1/5794)
[Wikitext](https://en.wikisource.org/wiki/Scheme:_An_Interpreter_for_Extended_Lambda_Calculus/Whole_text)

### Lambda: The Ultimate Imperative

[DSpace](https://dspace.mit.edu/handle/1721.1/5790)
[Wikisource](https://en.wikisource.org/wiki/Lambda:_The_Ultimate_Imperative)

### Lambda: The Ultimate Declarative

[DSpace](https://dspace.mit.edu/handle/1721.1/6091)
[Wikisource](https://en.wikisource.org/wiki/Lambda:_The_Ultimate_Declarative)

### Lambda: The Ultimate GOTO

[DSpace](https://dspace.mit.edu/handle/1721.1/5753)

### The Art of the Interpreter

[DSpace](https://dspace.mit.edu/handle/1721.1/6094)

https://en.wikisource.org/wiki/Lambda_Papers

### Rabbit: A Compiler for Scheme

[DSpace](https://dspace.mit.edu/handle/1721.1/6913)
[Wikisource](https://en.wikisource.org/wiki/Rabbit:_A_Compiler_for_Scheme)

### Lambda: The Ultimate Opcode

[dSpace](https://dspace.mit.edu/handle/1721.1/5731)


## Other things

- [SICP](https://sarabander.github.io/sicp/)
- [Daniel P. Friedman](https://library-search.imperial.ac.uk/discovery/search?query=creator,exact,Friedman,%20Daniel%20P.&tab=Everything&search_scope=MyInst_and_CI&vid=44IMP_INST:ICL_VU1&facet=creator,exact,Friedman,%20Daniel%20P.&offset=0)
- [HTDP](https://htdp.org/2021-11-15/Book/index.html)
- [PLAI](https://cs.brown.edu/courses/cs173/2012/book/)
- [EOPL](https://library-search.imperial.ac.uk/discovery/fulldisplay?docid=cdi_crossref_primary_10_1017_S0956796809007357&context=PC&vid=44IMP_INST:ICL_VU1&lang=en&search_scope=MyInst_and_CI&adaptor=Primo%20Central&tab=Everything&query=any,contains,Essentials%20of%20Programming%20Languages&offset=0)
- [Lisp in small pieces](https://library-search.imperial.ac.uk/discovery/fulldisplay?docid=alma995336584401591&context=L&vid=44IMP_INST:ICL_VU1&lang=en&search_scope=MyInst_and_CI&adaptor=Local%20Search%20Engine&tab=Everything&query=any,contains,lisp%20in%20small%20pieces&offset=0)